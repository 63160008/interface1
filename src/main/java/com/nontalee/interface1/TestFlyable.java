/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.interface1;

/**
 *
 * @author nonta
 */
public class TestFlyable {

    public static void main(String[] args) {
        Bat bat = new Bat();
        Airplane airplane = new Airplane("Engine number 1");
        bat.fly();
        airplane.fly();
        Dog dog = new Dog();

        Flyable[] flyable = {bat, airplane};
        for (Flyable f : flyable) {
            if (f instanceof Airplane) {
                Airplane p = (Airplane) f;
                p.startengine();
                p.run();
            }
            f.fly();
        }
        Runable[] runable = {dog, airplane};
        for (Runable r : runable) {
            r.run();
        }
    }
}
