/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.interface1;

/**
 *
 * @author nonta
 */
public class Airplane extends Vehicle implements Flyable, Runable {

    public Airplane(String engine) {
        super(engine);
    }

    @Override
    public void startengine() {
        System.out.println("Airplane startengine");
    }

    @Override
    public void stoptengine() {
        System.out.println("Airplane stopengine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Airplane raiseSpeed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Airplane applyBreak");
    }

    @Override
    public void fly() {
        System.out.println("Airplane fly");
    }

    @Override
    public void run() {
        System.out.println("Airplane run");
    }

}
