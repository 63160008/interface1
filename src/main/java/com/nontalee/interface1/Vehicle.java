/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.interface1;

/**
 *
 * @author nonta
 */
public abstract class Vehicle {
    private String engine;

    public Vehicle(String engine) {
        this.engine = engine;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }
    
    public abstract void startengine();
    public abstract void stoptengine();
    public abstract void raiseSpeed();
    public abstract void applyBreak();
}
